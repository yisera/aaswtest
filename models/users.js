'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    UserName: DataTypes.STRING,
    UserEmail: DataTypes.STRING
  }, {});
  Users.associate = function(models) {
    models.Users.hasMany(models.Expenses);
  };
  return Users;
};