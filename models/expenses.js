'use strict';
module.exports = (sequelize, DataTypes) => {
  const Expenses = sequelize.define('Expenses', {
    ExpenseName: DataTypes.STRING,
    ExpenseMotive: DataTypes.STRING,
    ExpenseAmount: DataTypes.DECIMAL(13,4)
  }, {});
  Expenses.associate = function(models) {
    models.Expenses.belongsTo(models.ExpensesCategories, { foreignKey: "CategoryId" });
    models.Expenses.belongsTo(models.Users, { foreignKey:"UserId" });
  };
  return Expenses;
};