'use strict';
module.exports = (sequelize, DataTypes) => {
  const ExpensesCategories = sequelize.define('ExpensesCategories', {
    CategoryName: DataTypes.STRING,
    CategoryDescription: DataTypes.STRING
  }, {});
  ExpensesCategories.associate = function(models) {
    models.ExpensesCategories.hasMany(models.Expenses);
  };
  return ExpensesCategories;
};