'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('Expenses', ['CategoryId'], {
        type: 'foreign key',
        name: 'fk_expenses_for_category',
        references: {
            table: 'ExpensesCategories',
            field: 'id'
        }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
    .removeConstraint({ tableName: 'Expenses', constraintName: 'fk_expenses_for_user' })
    .removeConstraint({ tableName: 'Expenses', constraintName: 'fk_expenses_for_category' });
  }
};
