'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Expenses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ExpenseName: {
        type: Sequelize.STRING
      },
      ExpenseMotive: {
        type: Sequelize.STRING
      },
      ExpenseAmount: {
          type: Sequelize.DECIMAL(13,4)
      },
      CategoryId: {
        type: Sequelize.INTEGER,
        references: {
            model: 'ExpenseCategories',
            key: 'id'
        }
      },
      UserId: {
        type: Sequelize.INTEGER,
        references: {
            model: 'Users',
            key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Expenses');
  }
};