const { GraphQLObjectType } = require('graphql');

const userQueries = require('./users');
const expenseCategoryQueries = require('./expensecategories');


module.exports = new GraphQLObjectType({
    name: 'Query',
    fields: () => {
        return {
            user: userQueries.user,
            expenseCategories: expenseCategoryQueries.expenseCategories
        };
    }
});