const { GraphQLInt } = require('graphql');
const userType = require('../types').Users;
const { userController } = require('../../controllers');

module.exports.user = {
    type: userType,
    args: {
        id: { type: GraphQLInt }
    },
    resolve: (_, params) => {
        return new Promise( (resolve, reject) => {
            if(!(params.id)){
                const errorMessage = "You must specify at least one of the following parameters: id, email";
                return reject(errorMessage);
            } else {
                userController.getUserById(params.id)
                .then((userData) => {
                    if(!userData){
                        return resolve(null);
                    }
                    return resolve(userController.createGraphQLUserFormat(userData));
                })
                .catch( error => reject(error) );
            }
        });
    }
};