const { GraphQLInt, GraphQLList } = require('graphql');
const expensesCategoryType = require('../types/expensecategories').ExpensesCategories;

const { expensesCategoryController } = require('../../controllers');

module.exports.expenseCategories = {
    type: new GraphQLList(expensesCategoryType),
    args: {
        id: { type: GraphQLInt }
    },
    resolve: (_, params) => {
        return new Promise( (resolve, reject) => {
            if(!params.id){
                expensesCategoryController.getAllExpenseCategories()
                .then( categories => {
                    if(!categories) {
                        return resolve(null);
                    }
                    return resolve(
                        categories.map(expensesCategoryController.createGraphQLExpenseCategoryFormat)
                    );
                })
                .catch( error => reject(error) );
            } else {
                expensesCategoryController.getExpenseCategoryById(params.id)
                .then( category => {
                    if(!category) {
                        return resolve(null);
                    }
                    return resolve(
                        [expensesCategoryController.createGraphQLExpenseCategoryFormat(category)]
                    );
                })
                .catch( error => reject(error) );
            }
        });
    }
}

