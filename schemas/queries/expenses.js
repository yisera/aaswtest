const { GraphQLInt, GraphQLString, GraphQLList, GraphQLNonNull } = require('graphql');
const { GraphQLDateTime } = require('graphql-iso-date');

const ExpenseType = require('../types').Expenses;
const { expensesController, userController } = require('../../controllers');

module.exports.Expenses = {
    type: new GraphQLList(ExpenseType),
    args: {
        userId: { type: new GraphQLNonNull(GraphQLInt) },
    },
    resolve: (_, params) => {
        return new Promise( (resolve, reject) => {
            if(!params.userId) {
                let errorMessage = `No user was specified`;
                reject(errorMessage);
            } else {
                const userData = userController
                .getUserById(params.userId)
                .then( data => resolve(userController.createGraphQLUserFormat(data)))
                .catch( error => reject(error) );
                if(!userData){
                    return resolve(null);
                } else {
                    expensesController.getExpensesByUser(params.userId)
                    .then( data => {
                        if(!data){
                            return resolve(null);
                        } else {
                            data.push(userData);
                            return resolve(
                                data.map(expensesController.createExpensesByUserGraphQLFormat)
                            );
                        }
                    })
                    .catch( error => reject(error));
                }
            }
        });
    }
}