const { ExpensesCategories } = require('../../models');
const { GraphQLString, GraphQLInt, GraphQLNonNull } = require('graphql');
const ExpensesCategoryType = require('../types/expensecategories').ExpensesCategories;

exports.updateOrCreateExpenseCategory = {
    type: ExpensesCategoryType,
    args: {
        id: {
            type: GraphQLInt
        },
        categoryName: {
            type: new GraphQLNonNull(GraphQLString)
        },
        categoryDescription: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolve(root, params) {
        return new Promise( (resolve, reject) => {
            if(!params.id) {
                ExpensesCategories.create({
                    CategoryName: params.categoryName,
                    CategoryDescription: params.categoryDescription,
                    createdAt: new Date(),
                    updatedAt: new Date()
                })
                .then( newExpenseCategory => {
                    let newExpenseCategoryJson = {
                        id: newExpenseCategory.id,
                        categoryName: newExpenseCategory.CategoryName,
                        categoryDescription: newExpenseCategory.CategoryDescription
                    };
                    resolve(newExpenseCategoryJson);
                })
                .catch( error => reject(error) );
            } else {
                let query = { 'id': params.id };
                ExpensesCategories.find({
                    where: query,
                    limit: 1
                })
                .then( expenseCategory => {
                    if(expenseCategory) {
                        expenseCategory.CategoryName = params.categoryName ? params.categoryName : expenseCategory.CategoryName;
                        expenseCategory.CategoryDescription = params.categoryDescription ? params.categoryDescription : expenseCategory.CategoryDescription;
                        expenseCategory.updatedAt = new Date();
                        expenseCategory.save()
                        .then( updatedExpenseCategory => {
                            let updatedExpenseCategoryJson = {
                                id: updatedExpenseCategory.id,
                                categoryName: updatedExpenseCategory.CategoryName,
                                categoryDescription: updatedExpenseCategory.CategoryDescription
                            }
                            resolve(updatedExpenseCategoryJson);
                        })
                        .catch(error => reject(error));
                    } else {
                        let errorMessage = `Category with id ${params.id} does not exist.`;
                        reject(errorMessage);
                    }
                });
            }
        });
    }
}