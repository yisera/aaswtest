const { Users } = require('../../models');
const { GraphQLNonNull, GraphQLString, GraphQLInt } = require('graphql');
const UserType = require('../types/users').Users;

exports.updateOrCreateUser = {
    type: UserType,
    args: {
        id: {
            type: GraphQLInt
        },
        userName: {
            type: GraphQLString
        },
        userEmail: {
            type: GraphQLString
        }
    },
    resolve(root, params) {
        return new Promise( (resolve, reject) => {
            if(!params.id){
                query = { 'UserEmail': params.userEmail }
            } else {
                query = { 'id': params.id }
            }
            Users.find({
                where: query,
                limit: 1
            }).then( user => {
                if(!user) {
                    Users.create({
                        UserName: params.userName,
                        UserEmail: params.userEmail,
                        createdAt: new Date(),
                        updatedAt: new Date()
                    }).then( newUser => {
                        let newUserJson = {
                            id: newUser.id,
                            userName: newUser.UserName,
                            userEmail: newUser.UserEmail
                        };
                        resolve(newUserJson);
                    });
                } else {
                    user.id = params.id ? params.id : user.id;
                    user.UserName = params.userName ? params.userName : user.UserName;
                    user.UserEmail = params.userEmail ? params.userEmail : user.UserEmail;
                    user.updatedAt = new Date();
                    user.save().then( updatedUser => {
                        console.log(updatedUser);
                        let updatedUserJson = {
                            id: updatedUser.id,
                            userName: updatedUser.UserName,
                            userEmail: updatedUser.UserEmail
                        };
                        resolve(updatedUserJson);
                    });
                }
            })
        });
    }
}