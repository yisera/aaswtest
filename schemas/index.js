const { GraphQLSchema, GraphQLObjectType } = require('graphql');
const queryType = require('./queries');
const mutation = require('./mutations/index');

module.exports = new GraphQLSchema({
    query: queryType,
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: mutation
    })
});