const { Users } = require('./users');
const { Expenses } = require('./expenses');
const { ExpensesCategories } = require('./expensecategories');

module.exports = {
    Users,
    Expenses,
    ExpensesCategories
};