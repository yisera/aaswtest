const graphql = require('graphql');
const { GraphQLObjectType } = require('graphql');

const ExpensesCategories = new GraphQLObjectType({
    name: 'ExpensesCategories',
    description: 'ExpenseCategories GraphQL Object Schema Model',
    fields: {
        id: { type: graphql.GraphQLInt },
        categoryName: { type: graphql.GraphQLString },
        categoryDescription: { type: graphql.GraphQLString }
    }
});

module.exports.ExpensesCategories = ExpensesCategories;