const { GraphQLInt, GraphQLString, GraphQLFloat, GraphQLObjectType, GraphQLList } = require('graphql');
const { GraphQLDateTime } = require('graphql-iso-date');
const userType = require('./users').Users;

const Expenses = new GraphQLObjectType({ 
    name: 'Expenses',
    description: 'Expenses GraphQL Object Schema Model',
    fields: {
        id: { type: GraphQLInt },
        expenseName: { type: GraphQLString },
        expenseMotive: { type: GraphQLString },
        expenseAmount: { type: GraphQLFloat },
        user: { type: new GraphQLList(userType) },
        categoryId: { type: GraphQLInt },
        updatedAt: { type: GraphQLDateTime },
        createdAt: { type: GraphQLDateTime }
    }
});

module.exports.Expenses = Expenses;