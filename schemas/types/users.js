const { GraphQLInt, GraphQLString } = require('graphql');
const { GraphQLObjectType } = require('graphql');
const { GraphQLDateTime } = require('graphql-iso-date');

const Users = new GraphQLObjectType({ 
    name: 'Users',
    description: 'Users GraphQL Object Schema Model',
    fields: {
        id: { type: GraphQLInt },
        userName: { type: GraphQLString },
        userEmail: { type: GraphQLString },
        createdAt: { type: GraphQLDateTime }
    }
});

 module.exports.Users = Users;