const express =  require('express');
const morgan = require('morgan');
const bp = require('body-parser');
const graphqlHTTP = require('express-graphql');

const schema = require('./schemas/index');

const app = express();

app.use(bp.json());
app.use(morgan('dev'));
app.use(require('cors')());

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: global,
    graphiql: true
}));

app.get("*", (req, res) => {
    res.status(200).send({ 
        message: "Welcome to AASWTEST API"
    });
});

module.exports = app;