module.exports = {
    PORT: process.env.PORT || 3030,
    DB: {
        DEV: {
            DBNAME  : process.env.DB_NAME     || 'aaswtestdb',
            DBHOST  : process.env.DB_HOST     || 'localhost:5432',
            USERNAME: process.env.DB_USERNAME || 'postgres',
            PASSWORD: process.env.DB_PASSWORD || 'hola123'
        }
    }
}