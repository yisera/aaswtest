const app = require('./app');
const config = require('./config');

app.listen(config.PORT, () => {
    console.log(`API is running on port #${config.PORT}`);
});