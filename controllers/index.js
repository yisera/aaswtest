const userController = require('./users');
const expensesCategoryController = require('./expensecategories');
const expensesController = require('./expenses');

module.exports = {
    userController,
    expensesCategoryController,
    expensesController
};