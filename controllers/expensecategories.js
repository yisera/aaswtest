const { ExpenseCategories } = require('../models');

module.exports = {
    getAllExpenseCategories(){
        return ExpenseCategories.findAll();
    },
    getExpenseCategoryById(id) {
        return ExpenseCategories.findById(id);
    },
    createGraphQLExpenseCategoryFormat(expenseCategory) {
        const dataFormat = {};
        dataFormat.id = expenseCategory.id;
        dataFormat.categoryName = expenseCategory.CategoryName;
        dataFormat.categoryDescription = expenseCategory.CategoryDescription;
        return dataFormat;
    }
}