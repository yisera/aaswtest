const { Expenses, db } = require('../models');


module.exports = {
    getExpensesByUser(id) {
        return db.sequelize
        .query(`
            SELECT "id", "ExpenseName", "ExpenseMotive", "UserId", "CategoryId", "ExpenseAmount", "createdAt"
            FROM "Expenses" as "Expenses"
            WHERE EXTRACT(YEAR FROM "createdAt") = EXTRACT(YEAR FROM NOW())
            AND EXTRACT(MONTH FROM "createdAt") = EXTRACT(MONTH FROM NOW())
            AND "UserId" = ${id}
            `,
            { type: db.sequelize.QueryTypes.RAW }
        );
    },
    createExpensesByUserGraphQLFormat(data) {
        let formatData = {};
        formatData.id = data.id;
        formatData.expenseName = data.ExpenseName;
        formatData.expenseDescription = data.ExpenseDescription;
        formatData.expenseAmount = data.ExpenseAmount;
        formatData.user = data.userData.map( user => {
            return {
                id: user.id,
                userName: user.UserName,
                userEmail: user.UserEmail
            };
        });
        formatData.categoryId = data.CategoryId;
        formatData.updatedAt = data.updatedAt;
        formatdata.createdAt = data.createdAt;
        console.log(formatData);
        return formatdata;
    }
};