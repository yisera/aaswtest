const { Users } = require('../models');

module.exports = {
    getUserById(id) {
        return Users.findById(id);
    },
    getUserByEmail(email){
        return Users.find({
            where: {
                UserEmail: email
             }
        });
    },
    createGraphQLUserFormat(user) {
        const dataFormat = {};
        dataFormat.id = user.id;
        dataFormat.userName = user.UserName;
        dataFormat.userEmail = user.UserEmail;
        return dataFormat;
    }
};